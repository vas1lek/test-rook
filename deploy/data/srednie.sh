#!/bin/bash


IFS=$'\n'
sum=0
d=0
file=`cat $1 | sed 's/MiB\/s//g'`

for i in $file 
do
    if echo $i | grep K >> /dev/null
    then
        i=`echo $i | sed 's/KiB\/s//g'`
        i=$(bc<<<"scale=3;$i/1000")
    fi
    sum=$(bc<<<"scale=3;$sum+$i")
    d=$[d+1]
done

results=$(bc<<<"scale=3;$sum/$d")
echo $sum
echo $d
echo $results

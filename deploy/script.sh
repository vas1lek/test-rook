#!/bin/bash

# Этот скрипт запускает тестирование 100 раз 


for n in {1..100}
do
    complete="start"
    kubectl apply -f fio.yaml
    while [ $complete != "Completed" ]
    do
        sleep 10
        echo "Job все еще выполняется"
        complete=`kubectl describe job dbench | tail -1| awk '{print $2}'`
    done

    kubectl logs jobs/dbench | tail -10 | awk -F: '{print $2}' > results.txt

    file="results.txt"
    IFS=$'\n'
    Numberfiles=1
    for i in $(cat $file) 
    do
        echo $i >> data/${Numberfiles}_*
        Numberfiles=$((Numberfiles+1))  
    done

    kubectl delete -f fio.yaml
    sleep 10
    echo "Тест под номером $n завершен, результаты записаны"

done


#| tail -10 | awk -F: '{print $2}'| sed 's/MiB\/s//g'
#| sed 's/MiB\/s//g' | sed 's/KiB\/s//g'
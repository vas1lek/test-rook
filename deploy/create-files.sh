#!/bin/bash

# Этот скрипт создает файлы для записей данных
# В эти файлы будут записываться данные

if [ -d data ]
then
    echo 
else
    mkdir data
fi

for i in 1_Random_Read.txt \
         2_Random_Write.txt \
         3_BW_Read.txt \
         4_BW_Write.txt \
         5_Average_Latency_Read.txt \
         6_Average_Latency_Write.txt \
         7_Sequential_Read.txt \
         8_Sequential_Write.txt \
         9_Mixed_Random_Read.txt \
         10_Mixed_Random_Write.txt 
do
    touch data/$i 
done